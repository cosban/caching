package caching

import (
	"github.com/google/uuid"
	"gitlab.com/cosban/assert"
	"testing"
)

type testItem struct {
	pk uuid.UUID
	g1 int64
	g2 int64
}

func (t testItem) ID() uuid.UUID {
	return t.pk
}

func TestFromID(t *testing.T) {
	given := NewFIFO[uuid.UUID, testItem]()
	item1 := testItem{uuid.New(), 3, 4}
	item2 := testItem{uuid.New(), 8, 7}
	item3 := testItem{uuid.New(), 6, 2}
	given.Add(item1, item2, item3)

	scenarios := []struct {
		name     string
		input    uuid.UUID
		expected testItem
		truthy   bool
	}{
		{"Invalid", uuid.New(), testItem{}, false},
		{"Item One", item1.pk, item1, true},
		{"Item Two", item2.pk, item2, true},
		{"Item Three", item3.pk, item3, true},
	}

	for _, scenario := range scenarios {
		t.Run(scenario.name, func(t *testing.T) {
			actual, ok := given.FromID(scenario.input)
			assert.Equals(t, scenario.truthy, ok)
			assert.Equals(t, scenario.expected, actual)
		})
	}
}

func TestGetByIndex(t *testing.T) {
	given := NewFIFO[uuid.UUID, testItem]()
	item1 := testItem{uuid.New(), 3, 4}
	item2 := testItem{uuid.New(), 8, 7}
	item3 := testItem{uuid.New(), 6, 2}
	given.Add(item1, item2, item3)

	scenarios := []struct {
		name     string
		input    int
		expected testItem
		truthy   bool
	}{
		{"Too High", 3, testItem{}, false},
		{"Too Low", -1, testItem{}, false},
		{"Zero", 0, item1, true},
		{"One", 1, item2, true},
		{"Two", 2, item3, true},
	}

	for _, scenario := range scenarios {
		t.Run(scenario.name, func(t *testing.T) {
			actual, ok := given.Get(scenario.input)
			assert.Equals(t, scenario.truthy, ok)
			assert.Equals(t, scenario.expected, actual)
		})
	}
}

func TestGetLRUByIndex(t *testing.T) {
	given := NewLRU[uuid.UUID, testItem]()
	item1 := testItem{uuid.New(), 3, 4}
	item2 := testItem{uuid.New(), 8, 7}
	item3 := testItem{uuid.New(), 6, 2}
	given.Add(item1, item2, item3)

	scenarios := []struct {
		name     string
		input    int
		expected testItem
		truthy   bool
	}{
		{"Too High", 3, testItem{}, false},
		{"Too Low", -1, testItem{}, false},
		{"First added", 0, item1, true},
		{"Second added", 0, item2, true},
		{"Third added", 0, item3, true},
	}

	for _, scenario := range scenarios {
		t.Run(scenario.name, func(t *testing.T) {
			actual, ok := given.Get(scenario.input)
			assert.Equals(t, scenario.truthy, ok)
			assert.Equals(t, scenario.expected, actual)
		})
	}
}

func TestAddGroupings(t *testing.T) {
	given := NewFIFO[int64, testItem]()
	given.GroupingBy("g1", func(index int, ordered []testItem) (int64, bool) {
		return ordered[index].g1, true
	})
	given.GroupingBy("g2", func(index int, ordered []testItem) (int64, bool) {
		return ordered[index].g2, true
	})
}

func TestFIFOEvictsByOrderAdded(t *testing.T) {
	given := NewFIFO[int64, testItem]().WithCap(1)
	given.GroupingBy("g1", func(i int, ordered []testItem) (int64, bool) {
		return ordered[i].g1, true
	})
	item1 := testItem{uuid.New(), 3, 4}
	item2 := testItem{uuid.New(), 8, 7}

	given.Add(item1)
	actual, ok := given.Get(0)
	assert.True(t, ok)
	assert.Equals(t, item1, actual)

	given.Add(item2)
	actual, ok = given.Get(0)
	assert.True(t, ok)
	assert.Equals(t, item2, actual)
}

func TestCacheKeepsTrackOfAccess(t *testing.T) {
	given := NewLRU[int64, testItem]()
	given.GroupingBy("g1", func(i int, ordered []testItem) (int64, bool) {
		return ordered[i].g1, true
	})
	item1 := testItem{uuid.New(), 3, 4}
	item2 := testItem{uuid.New(), 8, 7}
	item3 := testItem{uuid.New(), 6, 2}
	given.Add(item1, item2, item3)

	scenarios := []struct {
		name   string
		input  int64
		order  []testItem
		truthy bool
	}{
		{"First is oldest when last is picked", 6, []testItem{item1, item2, item3}, true},
		{"Second is oldest when first is picked", 3, []testItem{item2, item3, item1}, true},
		{"Third is oldest when second is picked", 8, []testItem{item3, item1, item2}, true},
		{"Invalid item", 12, []testItem{item3, item1, item2}, false},
	}

	for _, scenario := range scenarios {
		t.Run(scenario.name, func(t *testing.T) {
			_, ok := given.From("g1", scenario.input)
			assert.Equals(t, scenario.truthy, ok)
			assert.AreIdentical(t, scenario.order, given.ordered)
		})
	}
}
