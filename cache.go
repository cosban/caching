package caching

import (
	"github.com/google/uuid"
)

type Identifiable interface {
	ID() uuid.UUID
}

type GroupedCache[K comparable, V Identifiable] struct {
	// key to map of specific id type to ordered index
	by *Map[string, grouping[K, V]]
	// primary key to index
	pk *Map[uuid.UUID, *int]
	// maximum size
	cap int
	// called when an item is retrieved
	getItem func(index int, ordered *[]V) V
	// ordered list of items
	ordered []V
}

type grouping[K comparable, V Identifiable] struct {
	tracer func(int, []V) (K, bool)
	values map[K]int
}

func newCache[K comparable, V Identifiable]() *GroupedCache[K, V] {
	return &GroupedCache[K, V]{
		cap:     100,
		by:      NewMap[string, grouping[K, V]](),
		pk:      NewMap[uuid.UUID, *int](),
		ordered: []V{},
	}
}

func NewFIFO[K comparable, V Identifiable]() *GroupedCache[K, V] {
	g := newCache[K, V]()
	g.getItem = func(index int, ordered *[]V) V {
		return (*ordered)[index]
	}
	return g
}

func NewLRU[K comparable, V Identifiable]() *GroupedCache[K, V] {
	g := newCache[K, V]()
	g.getItem = func(index int, ordered *[]V) V {
		v := (*ordered)[index]
		*ordered = append((*ordered)[:index], (*ordered)[index+1:]...)
		*ordered = append(*ordered, v)
		lastIndex := len(*ordered) - 1
		g.pk.Put(v.ID(), &lastIndex)
		g.by.Range(func(key string, value grouping[K, V]) bool {
			id, _ := value.tracer(0, *ordered)
			value.values[id] = 0
			return true
		})
		return v
	}
	return g
}

func (p *GroupedCache[K, V]) WithCap(c int) *GroupedCache[K, V] {
	p.cap = c
	return p
}

func (p *GroupedCache[K, V]) GroupingBy(key string, tracer func(int, []V) (K, bool)) *GroupedCache[K, V] {
	p.by.Put(key, grouping[K, V]{
		tracer: tracer,
		values: map[K]int{},
	})
	return p
}

func (p *GroupedCache[K, V]) Add(items ...V) {
	for _, u := range items {
		p.ordered = append(p.ordered, u)
		index := len(p.ordered) - 1
		p.pk.Put(u.ID(), &index)
		p.by.Range(func(key string, value grouping[K, V]) bool {
			if id, ok := value.tracer(index, p.ordered); ok {
				value.values[id] = index
				p.by.Put(key, value)
			}
			return true
		})
	}
	for len(p.ordered) > p.cap {
		p.by.Range(func(key string, value grouping[K, V]) bool {
			if id, ok := value.tracer(0, p.ordered); ok {
				delete(value.values, id)
				p.by.Put(key, value)
			}
			return true
		})
		p.ordered = append(p.ordered[:0], p.ordered[1:]...)
	}
}

func (p *GroupedCache[K, V]) Get(index int) (V, bool) {
	if index >= len(p.ordered) || index < 0 {
		var o V
		return o, false
	}
	return p.getItem(index, &p.ordered), true
}

func (p *GroupedCache[K, V]) FromID(id uuid.UUID) (V, bool) {
	if index, ok := p.pk.Get(id); ok {
		return p.Get(*index)
	}
	var out V
	return out, false
}

func (p *GroupedCache[K, V]) From(grouping string, key K) (V, bool) {
	if m, ok := p.by.Get(grouping); ok {
		if index, ok := m.values[key]; ok {
			return p.Get(index)
		}
	}
	var out V
	return out, false
}
