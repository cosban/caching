package caching

import "sync"

type Map[K comparable, V any] struct {
	delegate *sync.Map
}

func NewMap[K comparable, V any]() *Map[K, V] {
	return &Map[K, V]{
		delegate: &sync.Map{},
	}
}

func (m *Map[K, V]) Get(key K) (V, bool) {
	value, ok := m.delegate.Load(key)
	if !ok {
		var out V
		return out, ok
	}
	return value.(V), ok
}

func (m *Map[K, V]) Put(key K, value V) *Map[K, V] {
	m.delegate.Store(key, value)
	return m
}

func (m *Map[K, V]) Remove(key K) (V, bool) {
	value, ok := m.delegate.LoadAndDelete(key)
	return value.(V), ok
}

func (m *Map[K, V]) Range(r func(key K, value V) bool) {
	m.delegate.Range(func(k any, v any) bool {
		return r(k.(K), v.(V))
	})
}
