module gitlab.com/cosban/caching

go 1.18

require (
	github.com/google/uuid v1.3.0
	gitlab.com/cosban/assert v0.0.0-20210125184416-11040dc6a192
)
